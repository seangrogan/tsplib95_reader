#include "tsplib95_distance_functions.h"

//  Euclidean distance (L2-metric)
double euclidean_distance(node &n1, node &n2) {
	double xd = n1.x - n2.x;
	double yd = n1.y - n2.y;
	double zd = n1.z - n2.z;
	return pow(xd*xd + yd * yd + zd * zd, 0.5); 
}
double euclidean_distance_2d(node &n1, node &n2) {
	double xd = n1.x - n2.x;
	double yd = n1.y - n2.y;
	return pow(xd*xd + yd * yd, 0.5);
}
double euclidean_distance_3d(node &n1, node &n2) {
	return euclidean_distance(n1, n2);
}

// Manhattan distance (L1-metric)
double manhattan_distance(node &n1, node &n2) {
	double xd = n1.x - n2.x;
	double yd = n1.y - n2.y;
	double zd = n1.z - n2.z;
	return abs(xd) + abs(yd) + abs(zd);
}
double manhattan_distance_2d(node &n1, node &n2) {
	double xd = n1.x - n2.x;
	double yd = n1.y - n2.y;
	return abs(xd) + abs(yd);
}
double manhattan_distance_3d(node &n1, node &n2) {
	return manhattan_distance(n1, n2);
}

//  Maximum distance (L_inf-metric)
double maximum_distance(node &n1, node &n2) {
	double xd = abs(n1.x - n2.x);
	double yd = abs(n1.y - n2.y);
	double zd = abs(n1.z - n2.z);
	return std::max(xd, yd, zd);
}
double maximum_distance_2d(node &n1, node &n2) {
	double xd = abs(n1.x - n2.x);
	double yd = abs(n1.y - n2.y);
	return std::max(xd, yd);
}
double maximum_distance_3d(node &n1, node &n2) {
	double xd = abs(n1.x - n2.x);
	double yd = abs(n1.y - n2.y);
	double zd = abs(n1.z - n2.z);
	return std::max(xd, yd, zd);
}

// Geographical distance
double geographical_distance(node &n1, node &n2) {
	double lat_n1 = get_latitude(n1);
	double lon_n1 = get_longitude(n1);
	double lat_n2 = get_latitude(n2);
	double lon_n2 = get_longitude(n2);
	double q1 = cos(lon_n1 - lon_n2);
	double q2 = cos(lat_n1 - lat_n2);
	double q3 = cos(lat_n1 + lat_n2);
	return (R_EARTH_KM * acos(0.5*((1.0 + q1)*q2 - (1.0 - q1)*q3)) + 1.0);
}
double get_latitude(node n) {
	double deg = (int) n.x;
	double min = n.x - deg;
	return PI * (deg + 5.0 * min / 3.0) / 180.0;
}
double get_longitude(node n) {
	double deg = (int) n.y;
	double min = n.y - deg;
	return PI * (deg + 5.0 * min / 3.0) / 180.0;
}

// Pseudo-Euclidean distance
double pseudo_euclidean_distance(node &n1, node &n2) {
	double xd = n1.x - n2.x;
	double yd = n1.y - n2.y;
	double rij = pow((xd*xd + yd * yd)/10.0, 0.5);
	int tij = (int)rij;
	if (tij < rij) return tij + 1;
	else return tij;
}

// Ceiling of the Euclidean distance
double ceil_euclidean_distance(node &n1, node &n2) {
	double xd = n1.x - n2.x;
	double yd = n1.y - n2.y;
	double zd = n1.z - n2.z;
	return ceil(pow(xd*xd + yd * yd + zd * zd, 0.5));
}

// Distance for crystallography problems
// TODO
// WAT???