#pragma once
// NAME
// Identifies the data file

// TYPE
// Specifies the type of the data. Possible types are
#define TSP "TSP"		// Data for a symmetric traveling salesman problem
#define ATSP "ATSP"		// Data for an asymmetric traveling salesman problem
#define SOP "SOP"		// Data for a sequential ordering problem
#define HCP "HCP"		// Hamiltonian cycle problem data
#define CVRP "CVRP"		// Capacitated vehicle routing problem data
#define TOUR "TOUR"		// A collection of tour

// COMMENT
// Additional comments (usually the name of the contributor or creator of the problem instance is given here).
// DIMENSION 
// For a TSP or ATSP, the dimension is the number of its nodes. For a CVRP, it is the total number of nodes and depots.For a TOUR file it is the dimension of the corresponding problem 

// CAPACITY 
// Specifies the truck capacity in a CVRP.

// EDGE WEIGHT TYPE 
// Specifies how the edge weights(or distances) are given.The values are
#define EXPLICIT "EXPLICIT"	// Weights are listed explicitly in the corresponding section
#define EUC_2D "EUC_2D"		// Weights are Euclidean distances in 2-D
#define EUC_3D "EUC_3D"		// Weights are Euclidean distances in 3-D
#define MAX_2D "MAX_2D"		// Weights are maximum distances in 2-D
#define MAX_3D "MAX_3D"		// Weights are maximum distances in 3-D
#define MAN_2D "MAN_2D"		// Weights are Manhattan distances in 2-D
#define MAN_3D "MAN_3D"		// Weights are Manhattan distances in 3-D
#define CEIL_2D "CEIL_2D"	// Weights are Euclidean distances in 2-D rounded up
#define GEO "GEO"			// Weights are geographical distances
#define ATT "ATT"			// Special distance function for problems att48 and att532
#define XRAY1 "XRAY1"		// Special distance function for crystallography problems(Version 1)
#define XRAY2 "XRAY2"		// Special distance function for crystallography problems(Version 2)
#define SPECIAL "SPECIAL"	// There is a special distance function documented elsewhere

// EDGE WEIGHT FORMAT
// Describes the format of the edge weights if they are given explicitly. The values are
#define FUNCTION "FUNCTION"				// Weights are given by a function(see above)
#define FULL_MATRIX "FULL_MATRIX"		// Weights are given by a full matrix
#define UPPER_ROW "UPPER_ROW"			// Upper triangular matrix(row - wise without diagonal entries)
#define LOWER_ROW "LOWER_ROW"			// Lower triangular matrix(row - wise without diagonal entries)
#define UPPER_DIAG_ROW "UPPER_DIAG_ROW"	// Upper triangular matrix(row - wise including diagonal entries)
#define LOWER_DIAG_ROW "LOWER_DIAG_ROW"	// Lower triangular matrix(row - wise including diagonal entries)
#define UPPER_COL "UPPER_COL"			// Upper triangular matrix(column - wise without diagonal entries)
#define LOWER_COL "LOWER_COL"			// Lower triangular matrix(column - wise without diagonal entries)
#define UPPER_DIAG_COL "UPPER_DIAG_COL"	// Upper triangular matrix(column - wise including diagonal entries)
#define LOWER_DIAG_COL "LOWER_DIAG_COL"	// Lower triangular matrix(column - wise including diagonal entries)

// EDGE DATA FORMAT
// Describes the format in which the edges of a graph are given, if the graph is not complete. The values are
#define EDGE_LIST "EDGE_LIST"		// The graph is given by an edge list
#define ADJ_LIST "ADJ_LIST"			// The graph is given as an adjacency list

// NODE COORD TYPE
 // Specifies whether coordinates are associated with each node(which, for example may be used for either graphical display or distance computations).The values are 
#define TWOD_COORDS "TWOD_COORDS"		// Nodes are specified by coordinates in 2 - D
#define THREED_COORDS "THREED_COORDS"	// Nodes are specified by coordinates in 3 - D
#define NO_COORDS "NO_COORDS"			// The nodes do not have associated coordinates