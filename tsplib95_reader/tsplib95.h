#pragma once
#include <string>
#include <vector>
#include "tsplib95_keys.h"

#define str std::string

struct node {
	int id;
	double x, y, z;
	z = 0;
};

class tsplib95
{
public:
	tsplib95();
	tsplib95(str tsp_file);
	tsplib95(std::ifstream tsp_file);
	~tsplib95();
protected:
	str NAME;
	str TYPE;
	std::vector<str> COMMENT;
	int DIMENSION;
	int CAPACITY; 
	str EDGE_WEIGHT_TYPE;
	str EDGE_WEIGHT_FORMAT;
	str EDGE_DATA_FORMAT;
	str NODE_COORD_TYPE;
	str	DISPLAY_DATA_TYPE;
	
	std::vector<node> NODE_COORD_SECTION;
	std::vector<node> DEPOT_SECTION;
	std::vector<int> DEMAND_SECTION;
	std::vector<std::vector<int>> EDGE_DATA_SECTION;
	std::vector<int> FIXED_EDGES_SECTION;
};

